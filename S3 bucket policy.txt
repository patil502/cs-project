{
    "Version": "2008-10-17",
    "Id": "Policy1342052196146",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::895773261951:role/solvezy-cis-audit-lambda-role"
            },
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::cis-solvezy-sec-report",
                "arn:aws:s3:::cis-solvezy-sec-report/*"
            ]
        }
    ]
}